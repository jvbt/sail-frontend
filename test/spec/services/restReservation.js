'use strict';

describe('Service: restReservation', function () {

  // load the service's module
  beforeEach(module('sailFrontendApp'));

  // instantiate service
  var restReservation;
  beforeEach(inject(function (_restReservation_) {
    restReservation = _restReservation_;
  }));

  it('should do something', function () {
    expect(!!restReservation).toBe(true);
  });

});
