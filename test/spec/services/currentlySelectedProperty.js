'use strict';

describe('Service: currentlySelectedProperty', function () {

  // load the service's module
  beforeEach(module('sailFrontendApp'));

  // instantiate service
  var currentlySelectedProperty;
  beforeEach(inject(function (_currentlySelectedProperty_) {
    currentlySelectedProperty = _currentlySelectedProperty_;
  }));

  it('should do something', function () {
    expect(!!currentlySelectedProperty).toBe(true);
  });

});
