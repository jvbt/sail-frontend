'use strict';

describe('Service: restProperty', function () {

  // load the service's module
  beforeEach(module('sailFrontendApp'));

  // instantiate service
  var restProperty;
  beforeEach(inject(function (_restProperty_) {
    restProperty = _restProperty_;
  }));

  it('should do something', function () {
    expect(!!restProperty).toBe(true);
  });

});
