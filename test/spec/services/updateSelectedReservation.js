'use strict';

describe('Service: updateSelectedReservation', function () {

  // load the service's module
  beforeEach(module('sailFrontendApp'));

  // instantiate service
  var updateSelectedReservation;
  beforeEach(inject(function (_updateSelectedReservation_) {
    updateSelectedReservation = _updateSelectedReservation_;
  }));

  it('should do something', function () {
    expect(!!updateSelectedReservation).toBe(true);
  });

});
