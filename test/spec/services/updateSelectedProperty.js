'use strict';

describe('Service: updateSelectedProperty', function () {

  // load the service's module
  beforeEach(module('sailFrontendApp'));

  // instantiate service
  var updateSelectedProperty;
  beforeEach(inject(function (_updateSelectedProperty_) {
    updateSelectedProperty = _updateSelectedProperty_;
  }));

  it('should do something', function () {
    expect(!!updateSelectedProperty).toBe(true);
  });

});
