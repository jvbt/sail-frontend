'use strict';

describe('Service: currentlySelectedReservation', function () {

  // load the service's module
  beforeEach(module('sailFrontendApp'));

  // instantiate service
  var currentlySelectedReservation;
  beforeEach(inject(function (_currentlySelectedReservation_) {
    currentlySelectedReservation = _currentlySelectedReservation_;
  }));

  it('should do something', function () {
    expect(!!currentlySelectedReservation).toBe(true);
  });

});
