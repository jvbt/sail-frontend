'use strict';

describe('Controller: ReservationdetailCtrl', function () {

  // load the controller's module
  beforeEach(module('sailFrontendApp'));

  var ReservationdetailCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ReservationdetailCtrl = $controller('ReservationdetailCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
