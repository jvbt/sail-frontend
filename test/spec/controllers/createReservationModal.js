'use strict';

describe('Controller: CreatereservationmodalCtrl', function () {

  // load the controller's module
  beforeEach(module('sailFrontendApp'));

  var CreatereservationmodalCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    CreatereservationmodalCtrl = $controller('CreatereservationmodalCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
