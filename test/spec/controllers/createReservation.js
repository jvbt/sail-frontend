'use strict';

describe('Controller: CreatereservationCtrl', function () {

  // load the controller's module
  beforeEach(module('sailFrontendApp'));

  var CreatereservationCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    CreatereservationCtrl = $controller('CreatereservationCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
