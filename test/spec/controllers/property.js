'use strict';

describe('Controller: PropertyCtrl', function () {

  // load the controller's module
  beforeEach(module('sailFrontendApp'));

  var PropertyCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PropertyCtrl = $controller('PropertyCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
