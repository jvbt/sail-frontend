'use strict';

describe('Controller: EditpropertymodalCtrl', function () {

  // load the controller's module
  beforeEach(module('sailFrontendApp'));

  var EditpropertymodalCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    EditpropertymodalCtrl = $controller('EditpropertymodalCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
