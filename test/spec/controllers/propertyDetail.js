'use strict';

describe('Controller: PropertydetailCtrl', function () {

  // load the controller's module
  beforeEach(module('sailFrontendApp'));

  var PropertydetailCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PropertydetailCtrl = $controller('PropertydetailCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
