'use strict';

describe('Controller: CreatepropertymodalCtrl', function () {

  // load the controller's module
  beforeEach(module('sailFrontendApp'));

  var CreatepropertymodalCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    CreatepropertymodalCtrl = $controller('CreatepropertymodalCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
