'use strict';

describe('Controller: EditreservationmodalCtrl', function () {

  // load the controller's module
  beforeEach(module('sailFrontendApp'));

  var EditreservationmodalCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    EditreservationmodalCtrl = $controller('EditreservationmodalCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
