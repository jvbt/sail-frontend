'use strict';

describe('Directive: myUiChart', function () {
  beforeEach(module('sailFrontendApp'));

  var element;

  it('should make hidden element visible', inject(function ($rootScope, $compile) {
    element = angular.element('<my-ui-chart></my-ui-chart>');
    element = $compile(element)($rootScope);
    expect(element.text()).toBe('this is the myUiChart directive');
  }));
});
