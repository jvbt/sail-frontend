'use strict';

angular.module('sailFrontendApp')
  .factory('currentlySelectedProperty', function () {
    // Service logic
    // ...

    // Public API here
    return {
      data: {}
    };
  });
