'use strict';

// This service is the exposed API to update the side panel and show the selected reservation.
// It relies on other services to perform the actual requests and caching.
angular.module('sailFrontendApp')
  .factory('updateSelectedReservation', ['currentlySelectedReservation', function (currentlySelectedReservation) {
    // Service logic
    // ...

    // Public API here
    return {
      update: function (reservation) {
        currentlySelectedReservation.data.reservation = reservation;
      }
    };
  }]);
