'use strict';

angular.module('sailFrontendApp')
  .factory('RestProperty', ['Restangular', '$cookies',
  function (Restangular, $cookies)
  {
    // Service logic
    var placeResource = 'places';
    var baseProperties = Restangular.all(placeResource);

    // Public API here
    var publicAPI =
    {
      all: baseProperties,
      
      // Should have more validation here
      post: function (newProperty)
      {
        var postPromise = baseProperties.post(newProperty, {}, {'X-CSRFToken': $cookies.csrftoken});
        return postPromise;
      },
      
      // Call patch to update objects. Returns a promise
      patch: function (RestObject, updatedProperties)
      {
        var patchPromise = RestObject.patch(updatedProperties, {}, {'X-CSRFToken': $cookies.csrftoken});
        return patchPromise;
      }
    };
    
    return publicAPI;
  }
  ]);
