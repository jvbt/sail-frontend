'use strict';

angular.module('sailFrontendApp')
  .factory('RestReservation', ['Restangular', '$cookies',
  function (Restangular, $cookies)
  {
    // Service logic
    var reservationResource = 'reservations';
    var baseReservations = Restangular.all(reservationResource);

    // Public API here
    var publicAPI =
    {
      all: baseReservations,
      
      // Should have more validation here
      post: function (newReservation)
      {
        var postPromise = baseReservations.post(newReservation,{},{'X-CSRFToken': $cookies.csrftoken});
        return postPromise;
      },
      
      // Call patch to update objects. Returns a promise
      patch: function (RestObject, updatedProperties)
      {
        var patchPromise = RestObject.patch(updatedProperties, {}, {'X-CSRFToken': $cookies.csrftoken});
        return patchPromise;
      }
    };
    
    return publicAPI;
  }
  ]);
