'use strict';

// This service is the exposed API to update the side panel and show the selected property.
// It relies on other services to perform the actual requests and caching.
angular.module('sailFrontendApp')
  .factory('updateSelectedProperty', ['currentlySelectedProperty', function (currentlySelectedProperty) {
    // Service logic
    // ...

    // Public API here
    return {
      update: function (property) {
        currentlySelectedProperty.data.property = property;
      }
    };
  }]);
