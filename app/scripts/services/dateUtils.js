'use strict';

angular.module('sailFrontendApp')
  .factory('DateUtils', function ()
  {
    // Service logic
    // ...

    // Public API here
    var publicAPI =
    { 
        /**
           This function is called after we get the date from the calendar.
           It does the following:
           - read the UTC date from the calendar
           - convert it to the browser's local timezone to see the user's selection
           - strip the timesone info
           - append the timezone selected by the user
           - convert the result to a UTC date string
        **/
        getUTCDateFromLocalDate: function(datestring, chosenTimezoneName)
        {
            // At this point the date is saved in UTC format by angularjs/the browser
            // Convert it back to the browser's timezone to see what was chosen by the user
            console.log("Initial UTC date string: "+ datestring);
            var utcDate = moment.parseZone(datestring);
            
            // Express date in browser's timezone to see the user's selection
            // Save date without the timezone
            var dateString = utcDate.local().format('YYYY-MM-DDTHH:mm:ss');
            console.log("Date in browser timezone with tz stripped: "+ dateString);
            
            // Parse date with zone intended by the user
            var actualDate = moment.tz(dateString, chosenTimezoneName);
            console.log("Date in user-selected timezone: "+ actualDate.format());
            
            // Now save date as UTC
            var UTCDate = actualDate.utc().format();
            console.log("Date string in UTC: "+ UTCDate);
            
            return UTCDate;
        },
        
        stripTimezoneFromDateObject: function(date)
        {
            var myDate = moment(date);
            var no_timezone_date_string = myDate.format('YYYY-MM-DDTHH:mm:ss');
            
            return no_timezone_date_string;
        },
        
        append_timezone_to_local_string_and_convert_to_UTC: function(local_date_string, chosenTimezoneName)
        {
            var myDate = moment.tz(local_date_string, chosenTimezoneName);
            var UTC_date_string = myDate.utc().format();
            
            return UTC_date_string;
        },
        
        // Assumption: The datepicker converts the date to the user's timezone, and saves it back as UTC.
        // We want the user to see the calendar info in his own timezone.
        // We need a way to let the user indicate the timezone and use that when displaying dates.
        getDateWithoutTimezone: function(dateString, chosenTimezoneName)
        {
            console.log('Timezone is '+ chosenTimezoneName);
            console.log("Initial date string: "+ dateString);
            // Parse string with zone information
            var actualDate = moment.parseZone(dateString);
            // Convert it to specified timezone
            var convertedDate = actualDate.tz(chosenTimezoneName);
            // Save date without the timezone
            // The browser will treat it as local time in the local time zone
            var simpleDateString = convertedDate.format('YYYY-MM-DDTHH:mm:ss');
            console.log("String in local zone: "+ simpleDateString);
        
            return simpleDateString;
        }
      
    };
    
    return publicAPI;
  });
