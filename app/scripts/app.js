'use strict';

angular.module('sailFrontendApp', ['restangular', 'ui.bootstrap.modal', 'ui.bootstrap.datepicker', 'ngCookies', 'ui.calendar', 'iso-4217-currency-codes'])
  .config(function ($routeProvider)
  {
    $routeProvider
      .when('/',
          {
            templateUrl: 'views/main.html',
            controller: 'MainCtrl'
          }
      )
      .otherwise(
          {
            redirectTo: '/'
          }
      );
  })
  .config(function (RestangularProvider)
  {
    RestangularProvider.setDefaultHeaders(
        {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    );
    RestangularProvider.setRequestSuffix('/');
    RestangularProvider.setBaseUrl('/api/v1');
    RestangularProvider.setRestangularFields(
        {
            selfLink: 'url'
        }
    );
  });
