'use strict';

angular.module('sailFrontendApp')
  .directive('myUiChart', function () {
    return {
      restrict: 'EACM',
      template: '<div></div>',
      replace: true,
      link: function (scope, elem, attrs) {
        var renderChart = function () {
          var data = scope.$eval(attrs.myUiChart);
          elem.html('');
          if (!angular.isArray(data)) {
            return;
          }

          var opts = {};
          if (!angular.isUndefined(attrs.chartOptions)) {
            opts = scope.$eval(attrs.chartOptions);
            if (!angular.isObject(opts)) {
              throw 'Invalid ui.chart options attribute';
            }
          }

          elem.jqplot(data, opts);
        };

        scope.$watch(attrs.myUiChart, function () {
          renderChart();
        }, false);

        scope.$watch(attrs.chartOptions, function () {
          renderChart();
        });
      }
    };
  });
