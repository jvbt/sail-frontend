'use strict';

angular.module('sailFrontendApp')
  .controller('ReportCtrl', ['$scope', 'Restangular', 'RestProperty', function ($scope, Restangular, RestProperty) {
    //
    // BUGBUG: use a service to cache list of properties.
    //
    
    var selectedProperty = {property:{}};
    $scope.selectedProperty = selectedProperty;
    
    //
    // Retrieve list of properties - (use a separate service to share the list later).
    //
    var propertiesPromise = RestProperty.all.getList();
    
    //
    // Expose list of properties.
    //
    $scope.propertyList = propertiesPromise;
    
    //
    // Use another controller to select report type?
    // What about date_from and date_to?
    //
    // Actually they could be chained in this file as different controllers. I don't want to put everything in the same controller.
    //
    
    //
    // The report type selected by the user will be bound to this variable.
    //
    var report = {type:{}, date_from:"", date_to:""};
    $scope.report = report;
    
    //
    // Expose list of report types.
    //
    var reportTypeList = [
        {type:"daily", name:"Daily"},
        {type:"weekly", name:"Weekly"},
        {type:"monthly", name:"Monthly"},
        {type:"quarterly", name:"Quarterly"},
        {type:"yearly", name:"Yearly"}
    ];
    $scope.reportTypeList = reportTypeList;

    //
    // Expose chart options.
    //
    $scope.chartOptions =
    {
	title: 'Daily Revenue',
	series:[{renderer:jQuery.jqplot.BarRenderer}],
	axes:
	{
	    xaxis:
	    {
		renderer: jQuery.jqplot.CategoryAxisRenderer,
		rendererOptions:
		{
		    tickRenderer: jQuery.jqplot.CanvasAxisTickRenderer
		}
	    }
	}
    };

    //
    // Return the report url for a given requency,
    // throw an exception if the frequency is invalid.
    //
    var getReportUrl = function(frequency, property)
    {
	var report_url = '';
	switch(frequency)
	{
	    case 'daily':
		report_url = property.daily_revenue_report_url;
		break;
	    default:
		throw new Error('Invalid report frequency ' + frequency + '.');
	}
	
        console.log('report_url is: ' + report_url);

	return report_url;
    };

    //
    // Get the report url from the property.
    //
    var getReportPromise = function(start_date, end_date, timezone_name, report_url)
    {
	//
	// Query parameters.
	//
	var query_params = 
	{
	    date_from: start_date,
	    date_to: end_date,
	    time_zone: timezone_name
	};

	//
	// Needs caching.
	//
	var report_promise = Restangular.oneUrl('', report_url).get(query_params);
	
	//
	// Done.
	//
	return report_promise;
    };

    //
    // Array indexes for consistent data handling.
    //
    var date_index = 0;
    var amount_index = 1;

    //
    // Creates the array of formatted dates and amounts that will be passed to the charting library.
    //
    var createFormattedDateArray = function(moment_start_date, moment_end_date, date_amount_array, date_format)
    {
	//
	// Number of input dates.
	//
	var nb_entries = date_amount_array.length;
	
	//
	// Expected output size.
	//
	var expected_final_length = moment_end_date.diff(moment_start_date, 'days') + 1;
	
	//
	// Output array.
	//
	var out = [];
	    
	//
	// Add entries of input array to output array.
	//
	for (var i = 0; i < nb_entries; i++)
	{
	    //
	    // Current item in input array.
	    //
	    var entry = date_amount_array[i];
	    
	    //
	    // Calculate index of item in output array.
	    //
	    var index = entry[date_index].diff(moment_start_date, 'day');
	    
	    //
	    // Safety check against moment_end_date.
	    //
	    if (index > (expected_final_length - 1))
	    {
		throw new Error("Input date is greater than moment_end_date. Input date is " + entry[date_index].format() + ", moment_end_date is " + moment_end_date.format() + ".");
	    }
	    
	    //
	    // Safety check against moment_start_date.
	    //
	    if (index < 0)
	    {
		throw new Error("Input date is smaller than moment_start_date. Input date is " + entry[date_index].format() + ", moment_start_date is " + moment_start_date.format() + ".");
	    }
	    
	    //
	    // Update output entry.
	    //
	    var res = [];
	    res[date_index] = entry[date_index].format(date_format);
	    //
	    // We convert the number from a string representation to a javascript number.
	    // It causes a precision loss.
	    // It is fine for charts but should not be used in tables to display info.
	    //
	    res[amount_index] = parseInt(entry[amount_index], 10);
	    out[index] = res;
	}
	
	//
	// Add remaining default values.
	//
	for (var j = 0; j < expected_final_length; j++)
	{
	    if (typeof out[j] == 'undefined')
	    {
		var res = [];
		res[date_index] = moment_start_date.clone().add('days', j).format(date_format);
		res[amount_index] = 0;
		out[j] = res;
	    }
	}
	
	//
	// Done.
	//
	console.log(out);
	return out;
    }

    //
    // Load report on button click.
    //
    $scope.loadReport = function()
    {
	//
	// getReportUrl throws if the frequency is invalid.
	//
	var report_url = getReportUrl(report.type.type, selectedProperty.property);

        
	//
	// Format to be used for the chart ticks.
	//
	var date_format = 'MMM D YYYY';


        // Get name of timezone chosen by the user.
        // Here we use the current timezone.
        // We should save the timezone once and re-use it.
        var timezone = jstz.determine();
        var timezone_name = timezone.name();

	//
	// Use user-selected time zone for dates.
	//
	var moment_date_from = moment.tz(report.date_from, timezone_name);
        var string_date_from = moment_date_from.format();

	var moment_date_to = moment.tz(report.date_to, timezone_name).endOf('day');
	var string_date_to = moment_date_to.format();

	//
	// Get a promise for the report data.
	//
	var report_promise = getReportPromise(string_date_from, string_date_to, timezone_name, report_url);
	
	//
	// Handle request success or failure.
	//
	report_promise.then(
	    function(report)
	    {
		// debug statement
		// Filter array to show only properties that are not functions
		var basedata = _.map(report,
		    function(res)
		    {
			return _.omit(res, _.isFunction)
		    }
		);
		
		console.log(basedata);

		//
		// Create sparse array with moment.js dates and associated amounts.
		//
                var date_amount_array = _.map(report,
		    function(element)
		    {
			var res = [];
			
			//
			// Moment.tz(a, b) constructor: if element[0] does not have a time zone, timezone_name is used.
			// Else the time zone of element[0] is used, then expressed in timezone_name.
			//
			// We are in the first case: element[0] is a naive date.
			//
			res[date_index] = moment.tz(element[0], timezone_name);
			
			res[amount_index] = element[1];
			
			return res;
		    }
		);
		
		//
		// Create full array that will be passed to the charting library.
		//
		var full_formatted_array = createFormattedDateArray(moment_date_from, moment_date_to, date_amount_array, date_format);
		
		//
		// Point scope variable of chart to full_array.
		//
		$scope.chartData = [full_formatted_array];
	    },
	    function(response)
	    {
		console.log('Response indicates an error: ', response, response.status);
	    }
	);
    };
 
  }])
