'use strict';

angular.module('sailFrontendApp')
  .controller('CreateReservationCtrl', ['$scope', 'currentlySelectedProperty', '$modal', function ($scope, currentlySelectedProperty, $modal) {
    
    $scope.openCreateReservationModal = function () {
      var modalInstance = $modal.open({
        templateUrl: 'createReservationModal.html',
        controller: 'CreateReservationModalCtrl',
        resolve: {
          selectedProperty: function () {
            return currentlySelectedProperty;
          }
        }
      });
    };
    
  }]);
