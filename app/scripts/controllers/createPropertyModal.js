'use strict';

angular.module('sailFrontendApp')
  .controller('CreatePropertyModalCtrl', ['$scope', 'RestProperty', '$modalInstance', 'ISO4217', function ($scope, RestProperty, $modalInstance, ISO4217) {
    
    // Later add a default currency matching the user's settings
    var newProperty =
    {
        name: "",
        description: ""
    }
    
    $scope.newProperty = newProperty;
    
    var allowedCurrencies = [];
    var currencyCodes = _.keys(ISO4217.codeToCurrency);
    var codeAndNamePairs = _.map(currencyCodes,
        function(code)
        {
            var entry = {};
            var value = ISO4217.codeToCurrency[code];
            entry.code = code;
            entry.name = value.name;
            allowedCurrencies.push(entry);
        }
    );
    
    $scope.currencyOptions = allowedCurrencies;
    
    $scope.ok = function() {
      //
      // Should have more validation here.
      //
      var postPromise = RestProperty.post(newProperty);
      postPromise.then(
          function(prop)
          {
              //
              // Here we should use the property list manager to insert the new item.
              // If not implemented yet, refresh to see the change.
              //
              $modalInstance.close(prop);
          },
          function(error)
          {
              console.log(error);
          }
      );
      
    }
    
    $scope.cancel = function() {
      $modalInstance.dismiss();
    }
    
  }]);
