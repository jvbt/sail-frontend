'use strict';

angular.module('sailFrontendApp')
  .controller('ReservationDetailCtrl', ['$scope', 'currentlySelectedReservation', '$modal', function ($scope, currentlySelectedReservation, $modal) {
    $scope.data = currentlySelectedReservation.data;
    
    $scope.open = function () {
      var modalInstance = $modal.open({
        templateUrl: 'editReservationModal.html',
        controller: 'EditReservationModalCtrl',
        resolve: {
          selectedReservation: function () {
            return currentlySelectedReservation;
          }
        }
      });
    };
    
  }]);
