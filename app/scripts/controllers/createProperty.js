'use strict';

angular.module('sailFrontendApp')
  .controller('CreatePropertyCtrl', ['$scope', '$modal', function ($scope, $modal) {
    
    $scope.openCreatePropertyModal = function () {
      var modalInstance = $modal.open({
        templateUrl: 'createPropertyModal.html',
        controller: 'CreatePropertyModalCtrl'
      });
    };
    
  }]);
