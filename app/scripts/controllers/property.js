'use strict';

angular.module('sailFrontendApp')
.controller('PropertyCtrl', ['$scope', 'RestProperty', 'updateSelectedProperty', function ($scope, RestProperty, updateSelectedProperty) {
    // Retrieve list of properties - (use a separate service to share the list later)
    var propertiesPromise = RestProperty.all.getList();
    
    // Expose list of properties
    $scope.properties = propertiesPromise;
    
    // Function to select a property
    $scope.selectProperty = function (property)
    {
        updateSelectedProperty.update(property);
    };
    
    // Property detail initialization
    // Set selected property to first item
    propertiesPromise
    .then(function(properties){
      // debug statement
      // Filter array to show only properties that are not functions
      var basedata = _.map(properties,
          function(property)
          {
              return _.omit(property, _.isFunction)
          }
      );
      console.log(basedata);
      
      $scope.selectProperty(properties[0])
    });
}]);
