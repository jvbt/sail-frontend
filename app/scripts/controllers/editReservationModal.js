'use strict';

// DatePicker bug: http://stackoverflow.com/questions/20217962/angular-ui-bootstrap-datepicker-not-displaying-over-bootstrap-modal

angular.module('sailFrontendApp')
  .controller('EditReservationModalCtrl', ['$scope', '$modalInstance', 'selectedReservation', 'RestReservation', 'DateUtils',
  function ($scope, $modalInstance, selectedReservation, RestReservation, DateUtils)
  {
    var publicFields = ['name', 'description', 'price', 'date_from', 'date_to'];
    var dateFields = ['date_from', 'date_to'];
    
    //
    // Utility functions
    //
    
    var copyFields = function(source, destination, fields)
    {
      _(fields).forEach(function(field)
      {
        destination[field] = _.cloneDeep(source[field]);
      });
    };
    
    // Utility function used to initialize date fields of a reservation object,
    // with dates in the browser timezone
    var initializeDateFields = function(reservation, dateFields, timezoneName)
    {
        _(dateFields).forEach(function(field)
        {
            var date_string = reservation[field];
            reservation[field] = DateUtils.getDateWithoutTimezone(date_string, timezoneName);
        });
    };
    
    // Utility function used to convert the calendar output to UTC
    var convertCalendarDateFields = function(reservation, dateFields, timezoneName)
    {
        _(dateFields).forEach(function(field)
        {
            var date_string = reservation[field];
            reservation[field] = DateUtils.getUTCDateFromLocalDate(date_string, timezoneName);
        });
    };
    
    //
    // Controller logic
    //
    
    var data = {reservation:{}};
    
    // Initialize reservation object for form
    copyFields(selectedReservation.data.reservation, data.reservation, publicFields);
    
    // Get name of timezone chosen by the user
    // Here we use the current timezone
    // We should save the timezone once and re-use it
    var timezone = jstz.determine();
    var timezoneName = timezone.name();
    
    // Retrieve local date instead of UTC date
    initializeDateFields(data.reservation, dateFields, timezoneName);
    
    // Data binding
    $scope.selectedReservation = data;
    
    $scope.ok = function() {
      // Should have more validation here
      
      convertCalendarDateFields(data.reservation, dateFields, timezoneName);
      
      var patchPromise = RestReservation.patch(selectedReservation.data.reservation, data.reservation);
      patchPromise.then(
          function(prop)
          {
              // Here we should use the reservation list manager to update the selected reservation
              // If not implemented yet, refresh to see the change
              $modalInstance.close(prop);
          },
          function(error)
          {
              console.log(error);
          }
      );
    };
    
    $scope.cancel = function() {
      $modalInstance.dismiss();
    };
    
  }]);
