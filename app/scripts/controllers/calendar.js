'use strict';

angular.module('sailFrontendApp')
    .controller('CalendarCtrl', ['$scope', 'RestReservation', 'DateUtils', 'Restangular', 'updateSelectedReservation', 
    function ($scope, RestReservation, DateUtils, Restangular, updateSelectedReservation)
    {
        // This function is triggered when a calendar event is clicked.
        // We update the selected reservation in the side panel.
        // For now we download the reservation. Later we'll use a cache.
        var calendarEventClick = function(event, jsEvent, view)
        {
            var reservationPromise = Restangular.oneUrl(null, event.rest_url).get();
            
            reservationPromise
            .then(function(reservation){
              // debug statement
              // Filter array to show only properties that are not functions
              var basedata = _.omit(reservation, _.isFunction);
              
              console.log(basedata);
              
              // Now we have the reservation.
              updateSelectedReservation.update(reservation);
              
            });
        };
        
        
        // Get name of timezone chosen by the user
        // Here we use the current timezone
        // We should save the timezone once and re-use it
        var timezone = jstz.determine();
        var timezoneName = timezone.name();
        
        var reservationToEvent = function(reservation)
        {
            var event = {};
            
            var event_start_string = DateUtils.getDateWithoutTimezone(reservation.date_from, timezoneName);
            var event_end_string = DateUtils.getDateWithoutTimezone(reservation.date_to, timezoneName);
            
            event.title = reservation.name;
            event.start = new Date(event_start_string);
            event.end = new Date(event_end_string);
            event.rest_url = reservation.url;
            
            return event;
        }
        
        var fetchCalendarEvent = function(start, end, callback)
        {
            // Get all reservations such that:
            // - their end date is greater than 'start'
            // - their start date is smaller than 'end'
            
            var local_start_string = DateUtils.stripTimezoneFromDateObject(start);
            var local_end_string = DateUtils.stripTimezoneFromDateObject(end);
            
            var start_string = DateUtils.append_timezone_to_local_string_and_convert_to_UTC(local_start_string, timezoneName);
            var end_string = DateUtils.append_timezone_to_local_string_and_convert_to_UTC(local_end_string, timezoneName);
            
            var reservationsPromise = RestReservation.all.getList({'min_date_to':start_string, 'max_date_from':end_string});
            
            reservationsPromise
            .then(function(reservations){
              // debug statement
              // Filter array to show only properties that are not functions
              var basedata = _.map(reservations,
                  function(res)
                  {
                      return _.omit(res, _.isFunction)
                  }
              );
              
              console.log(basedata);
              
              // Now we have reservations.
              // Convert them to events for the calendar.
              var events = _.map(reservations, reservationToEvent);
              callback(events);
              
            });
        }
        
        $scope.uiConfig =
        {
            calendar:
            {
                // Calendar config here
                eventClick: calendarEventClick
            }
        };
      
        $scope.eventSources =
        [
            {
                events: fetchCalendarEvent,
                ignoreTimezone: false
            }
        ];
        
        //
        // The following section could be wrapped in a service
        // to only re-render the calendar when needed.
        //
        // The advantage of re-rendering everytime we switch to the calendar
        // is automatically handling window size changes.
        //
        // This code could go in a directive.
        //
        
        var isCalendarVisible = function()
        {
            return $scope.mainCalendar.is(':visible');
        }

        var renderCalendar = function(newValue, oldValue)
        {
            if (newValue === oldValue)
            {
                // Initial run
                // Nothing to update
                return;
            }
            
            if (newValue === false)
            {
                // The calendar was visible and is now hidden
                // Nothing to update
                return;
            }
            
            // newValue is true
            // The calendar was hidden and is now visible
            // Re-render.
            //
            // The prev and next method calls do not work while the calendar is hidden.
            // But re-rendering will show new events, use new settings and update the size.
            $scope.mainCalendar.fullCalendar('render');
        }

        $scope.$watch(isCalendarVisible, renderCalendar);

    }]);
