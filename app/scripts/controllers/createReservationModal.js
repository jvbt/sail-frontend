'use strict';

angular.module('sailFrontendApp')
  .controller('CreateReservationModalCtrl', ['$scope', '$modalInstance', 'selectedProperty', 'RestReservation', 'DateUtils',
  function ($scope, $modalInstance, selectedProperty, RestReservation, DateUtils)
  {
    var publicFields = ['name', 'description', 'price', 'date_from', 'date_to'];
    var dateFields = ['date_from', 'date_to'];
    
    //
    // Utility function used to convert the calendar output to UTC.
    //
    var convertCalendarDateFields = function(reservation, dateFields, timezoneName)
    {
        _(dateFields).forEach(function(field)
        {
            var date_string = reservation[field];
            reservation[field] = DateUtils.getUTCDateFromLocalDate(date_string, timezoneName);
        });
    };
    
    //
    // Controller logic.
    //
    
    var newReservation =
    {
        name: "",
        description: ""
    }
    
    //
    // Get name of timezone chosen by the user
    // Here we use the current timezone
    // We should save the timezone once and re-use it
    //
    var timezone = jstz.determine();
    var timezoneName = timezone.name();
    
    //
    // Data binding.
    //
    $scope.newReservation = newReservation;
    $scope.selectedProperty = selectedProperty.data.property;
    
    $scope.ok = function() {
      //
      // Should have more validation here.
      //
      
      convertCalendarDateFields(newReservation, dateFields, timezoneName);
      newReservation.place = selectedProperty.data.property.url;
      
      var postPromise = RestReservation.post(newReservation);
      postPromise.then(
          function(prop)
          {
              //
              // Here we should use the reservation list manager to update the selected reservation.
              // If not implemented yet, refresh to see the change.
              //
              $modalInstance.close(prop);
          },
          function(error)
          {
              console.log(error);
          }
      );
    };
    
    $scope.cancel = function() {
      $modalInstance.dismiss();
    };
    
  }]);
