'use strict';

angular.module('sailFrontendApp')
.controller('ReservationCtrl', ['$scope', 'RestReservation', 'updateSelectedReservation', function ($scope, RestReservation, updateSelectedReservation) {
    // Retrieve list of reservations - (use a separate service for this later)
    var reservationsPromise = RestReservation.all.getList();
    
    // Expose list of reservations
    $scope.reservations = reservationsPromise;
    
    // Function to select a reservation
    $scope.selectReservation = function (reservation)
    {
        updateSelectedReservation.update(reservation);
    };
    
    // Reservation detail initialization
    // Set selected reservation to first item
    reservationsPromise
    .then(function(reservations){
      // debug statement
      // Filter array to show only properties that are not functions
      var basedata = _.map(reservations,
          function(res)
          {
              return _.omit(res, _.isFunction)
          }
      );
      
      console.log(basedata);
      
      $scope.selectReservation(reservations[0])
    });
}]);
