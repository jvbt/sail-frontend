'use strict';

angular.module('sailFrontendApp')
  .controller('EditPropertyModalCtrl', ['$scope', '$modalInstance', 'selectedProperty', 'RestProperty', 'ISO4217', function ($scope, $modalInstance, selectedProperty, RestProperty, ISO4217) {
    var publicFields = ['name', 'description', 'currency'];
    
    var copyFields = function(source, destination, fields)
    {
      _(fields).forEach(function(field)
      {
        destination[field] = _.cloneDeep(source[field]);
      });
    }
    
    var data = {property:{}}
    
    copyFields(selectedProperty.data.property, data.property, publicFields);
    
    $scope.selectedProperty = data;
    
    var currencyCodes = _.keys(ISO4217.codeToCurrency);
    var allowedCurrencies = _.map(currencyCodes,
        function(code)
        {
            var entry = {};
            var value = ISO4217.codeToCurrency[code];
            entry.code = code;
            entry.name = value.name;
            return entry;
        }
    );
    
    $scope.currencyOptions = allowedCurrencies;
    
    $scope.ok = function() {
      // Should have more validation here
      var patchPromise = RestProperty.patch(selectedProperty.data.property, data.property);
      patchPromise.then(
          function(prop)
          {
              // Here we should use the property list manager to update the selected property
              // If not implemented yet, refresh to see the change
              $modalInstance.close(prop);
          },
          function(error)
          {
              console.log(error);
          }
      );
    }
    
    $scope.cancel = function() {
      $modalInstance.dismiss();
    }
    
  }]);
