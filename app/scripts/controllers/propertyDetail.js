'use strict';

angular.module('sailFrontendApp')
  .controller('PropertyDetailCtrl', ['$scope', 'currentlySelectedProperty', '$modal', function ($scope, currentlySelectedProperty, $modal) {
    $scope.data = currentlySelectedProperty.data;
    
    $scope.open = function () {
      var modalInstance = $modal.open({
        templateUrl: 'editPropertyModal.html',
        controller: 'EditPropertyModalCtrl',
        resolve: {
          selectedProperty: function () {
            return currentlySelectedProperty;
          }
        }
      });
    };
    
  }]);
